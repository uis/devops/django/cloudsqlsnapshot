import pytest
from django.core.management import call_command

from cloudsqlsnapshot import SnapshotError


def test_disabled_by_default(db, mock_snapshot_client):
    "If disabled, no action is performed."
    call_command("migrate", "auth", "zero")
    mock_snapshot_client.assert_not_called()


def test_basic_functionality(
    db, enable_snapshots, mock_snapshot_client, project, location, instance
):
    call_command("migrate", "auth", "zero")

    # The snapshot client has the correct project.
    mock_snapshot_client.assert_called_once_with(project=project)

    # The snapshot was created.
    mock_snapshot_client.return_value.start.assert_called_once_with(
        instance=instance,
        description="Automated pre-migration backup",
        location=location,
    )

    # The operation was queried
    mock_snapshot_client.return_value.get_operation.assert_called_once_with(
        mock_snapshot_client.return_value.start.return_value["name"]
    )

    # The client was closed.
    mock_snapshot_client.return_value.close.assert_called_once()


def test_error(db, faker, enable_snapshots, mock_snapshot_client):
    "If an operation errors out, we raise a SnapshotError."
    message = faker.sentence()
    mock_snapshot_client.return_value.get_operation.return_value = {
        **mock_snapshot_client.return_value.get_operation.return_value,
        "error": {
            "errors": [{"message": message}],
        },
    }

    with pytest.raises(SnapshotError):
        call_command("migrate", "auth", "zero")
