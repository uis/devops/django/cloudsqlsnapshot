import os

import pytest
from django.conf import settings
from pytest_docker_tools import container

import cloudsqlsnapshot

# Create a PostgreSQL database container which will be used to run tests.
postgres_container = container(
    scope="session",
    image="postgres",
    environment={
        "POSTGRES_USER": "pytest-user",
        "POSTGRES_PASSWORD": "pytest-pass",
        "POSTGRES_DB": "pytest-db",
    },
    ports={
        "5432/tcp": None,
    },
)


@pytest.fixture(scope="session")
def django_db_modify_db_settings(django_db_modify_db_settings_parallel_suffix, postgres_container):
    """
    Modify database settings based on the postgres docker container we spun up.
    """
    host, port = postgres_container.get_addr("5432/tcp")
    db_settings = {
        "HOST": host,
        "PORT": port,
        "NAME": "pytest-db",
        "USER": "pytest-user",
        "PASSWORD": "pytest-pass",
    }

    for db_item in settings.DATABASES.values():
        db_item.update(db_settings)
        db_item.get("TEST", {}).update(db_settings)


@pytest.fixture
def project(faker):
    return f"proj-{faker.slug()}"


@pytest.fixture
def location(faker):
    return f"location-{faker.slug()}"


@pytest.fixture
def instance(faker):
    return f"sql-{faker.slug()}"


@pytest.fixture
def cloudsql_socket_path(project, location, instance):
    return f"/cloudsql/{project}:{location}:{instance}"


@pytest.fixture
def enable_snapshots(db, mocker, cloudsql_socket_path, settings):
    # Note that we use the db fixture here to ensure that the database has been created and initial
    # migrations run before enabling snapshots. That means we can patch the settings.
    mocker.patch("cloudsqlsnapshot._get_db_host", side_effect=lambda: cloudsql_socket_path)
    mocker.patch.dict(os.environ, {"CLOUDSQLSNAPSHOT_ENABLED": "1"})


# We mark the mock SnapshotClient as autouse so that there is no way we can accidentally actually
# make Google API calls.
@pytest.fixture(autouse=True)
def mock_snapshot_client(mocker, faker):
    mock = mocker.patch("cloudsqlsnapshot.SnapshotClient")
    op_name = faker.slug()
    mock.return_value.start.return_value = {"name": op_name, "status": "PENDING"}
    mock.return_value.get_operation.return_value = {"name": op_name, "status": "DONE"}
    return mock


@pytest.fixture(autouse=True)
def reset_has_run_flag():
    # This fixture simply resets the "__hasrun__" attribute cached on cloudsqlsnapshot after tests
    # run so that subsequent tests will continue to run snapshots.

    yield

    if hasattr(cloudsqlsnapshot.cloudsqlsnapshot, "__hasrun__"):
        delattr(cloudsqlsnapshot.cloudsqlsnapshot, "__hasrun__")
