# Full database configuration is filled in via a pytest fixture.
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "CONN_MAX_AGE": 60,  # seconds
    },
}

# Apps used for testing.
INSTALLED_APPS = [
    "cloudsqlsnapshot",
    # Any apps which have migrations which can be used to test the library.
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
]

# Fake secret key.
SECRET_KEY = "not-a-secret"

# Avoid warning about autofields
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Avoid deprecation warning for Django<5.0.
USE_TZ = True
