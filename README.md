# Cloud SQL Snapshot

A Django [pre-migrate
signal](https://docs.djangoproject.com/en/5.0/ref/signals/#pre-migrate) receiver
which takes a snapshot of a Google Cloud SQL instance before executing any
migrations.

## Quick start

This package is hosted on `gitlab.developers.cam.ac.uk` and can be added to a
Django project as follows.

```bash
poetry source add --priority=explicit cloudsqlsnapshot https://gitlab.developers.cam.ac.uk/api/v4/projects/8457/packages/pypi/simple
poetry add --source=cloudsqlsnapshot cloudsqlsnapshot
```

Once installed, add `cloudsqlsnapshot` to the list of `INSTALLED_APPS` to enable
Cloud SQL snapshots each time the `manage.py migrate` command is executed.

```py
INSTALLED_APPS = [
    ...
    "ui",
    "api",
    "cloudsqlsnapshot",
]
```

## Configuration

By default, this package expects to be running in a Cloud Run instance with the
target Cloud SQL database [mounted as a unix
socket](https://cloud.google.com/sql/docs/mysql/connect-run#connect). If this is
the case then adding `cloudsqlsnapshot` to the list of `INSTALLED_APPS` and
setting the environment variable `CLOUDSQLSNAPSHOT_ENABLED="1"` should be the
only configuration required, providing the account executing the `migrate`
command has the [required permissions](#permissions).

However, the following environment variables are available to configure the
signal receiver should you be attempting to use the package in a different
setup.

| Name | Description |
| ---- | ----------- |
| `CLOUDSQLSNAPSHOT_ENABLED` | Enable the signal receiver to take Cloud SQL snapshots. By default snapshots will not be enabled. |
| `CLOUDSQLSNAPSHOT_PROJECT` | The Google Cloud project that the Cloud SQL instance is in. |
| `CLOUDSQLSNAPSHOT_LOCATION` | The location of the Cloud SQL instance e.g. europe-west2. |
| `CLOUDSQLSNAPSHOT_INSTANCE` | The name of the Cloud SQL instance. |
| `CLOUDSQLSNAPSHOT_DESCRIPTION` | A description for the on-demand snapshot object, defaults to "Automated pre-migration backup". |
| `CLOUDSQLSNAPSHOT_SLEEP_DURATION_SECS` | The number of seconds to sleep when waiting for the snapshot task to completed, defaults to 5. |
| `CLOUDSQLSNAPSHOT_TIMEOUT_DURATION_SECS` | The number of seconds to timeout after when waiting for the snapshot task to complete, defaults to 300. |

## Permissions

To take a snapshot of a Cloud SQL instance the user/service account executing
the `manage.py migrate` command will need to have been granted the
`cloudsql.backupRuns.create` permission for the instance.

Our `gcp-product-factory` configures a [`custom.cloudsql.backupCreator` custom
role](https://gitlab.developers.cam.ac.uk/uis/devops/infra/gcp-product-factory/-/blob/master/modules/workspace/iam.tf?ref_type=heads#L39)
which contains the `cloudsql.backupRuns.create` permission. This is the role
that our infrastructure repositories should use to assign permissions to the
Cloud Run service account. For example:

```tf
resource "google_project_iam_member" "cloudsql_backup" {
  project = local.project
  role    = "projects/${local.project}/roles/custom.cloudsql.backupCreator"
  member  = "serviceAccount:${module.webapp.service_account.email}"
}
```
