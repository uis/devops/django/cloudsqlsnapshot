# Changelog

## [0.2.0](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/compare/0.1.0...0.2.0) (2024-06-17)


### Features

* add basic functionality check ([d8217f3](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/d8217f33267131d4dac6a788bf58d1f6194e5117))
* add basic test harness ([bfae619](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/bfae619eee10982b3cec8e682b0470b6c8eb9aad))
* add black, isort and coverage configuration ([c130715](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/c130715984a8c8ea87d242363aef7ba536eaf740))
* add GitLab CI configuration ([de917d7](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/de917d79d39f36caf85e499d30e8cd534eb76562))
* add testing dependencies ([d9a76f8](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/d9a76f87783b00246279a535c563f7a1a7e07af6))
* move snapshot functionality to mockable client ([7fe7377](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/7fe737741bf2c83d678bb349e5e5deee854d6e19))


### Bug Fixes

* allow later Django versions to be installed ([3b29c01](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/3b29c01903b16e59c76d36fff5a038f7acaf027a))
* black formatting fixes ([9b0e7d9](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/9b0e7d9607f079e40ad966b77caa78aa344bcf02))
* do not raise Exception on error ([78daf92](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/78daf92398766c2c8cbb83ceb8a6c985b99d6d68))

## 0.1.0 (2024-06-05)


### Features

* add cloudsqlsnapshot module ([c6dd027](https://gitlab.developers.cam.ac.uk/uis/devops/django/cloudsqlsnapshot/commit/c6dd027e47ea5d91270625835bcfe6cc2bec5691))
