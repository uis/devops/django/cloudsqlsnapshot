import contextlib
import logging
import os
import time

from django.conf import settings
from django.db.models.signals import pre_migrate
from django.dispatch import receiver
from googleapiclient.discovery import build

logger = logging.getLogger(__name__)


class SnapshotError(RuntimeError):
    pass


@receiver(pre_migrate)
def cloudsqlsnapshot(sender, using, plan, **kwargs):
    enabled = os.environ.get("CLOUDSQLSNAPSHOT_ENABLED", "")

    # The plan will be an empty list if there are no migrations to run - we skip the snapshot if
    # this is the case.
    if enabled == "" or not plan:
        return

    # By default, the pre_migrate signal fires for each registered django application. This would
    # cause many unnecessary Cloud SQL snapshots/backups. Therefore, we set the __hasrun__
    # attribute to True after the first invocation, effectively disabling this job for subsequent
    # pre_migrate signals received.
    if hasattr(cloudsqlsnapshot, "__hasrun__") and cloudsqlsnapshot.__hasrun__:
        return
    cloudsqlsnapshot.__hasrun__ = True

    description = getattr(
        settings, "CLOUDSQLSNAPSHOT_DESCRIPTION", "Automated pre-migration backup"
    )
    timeout_duration_secs = getattr(settings, "CLOUDSQLSNAPSHOT_TIMEOUT_DURATION_SECS", 300)
    sleep_duration_secs = getattr(settings, "CLOUDSQLSNAPSHOT_SLEEP_DURATION_SECS", 5)

    # "using" refers to the alias of the database specified in the migrate command. We use that to
    # lookup the connection string in the app settings.
    db_host = _get_db_host()
    db_host_project, db_host_location, db_host_instance = db_host.replace("/cloudsql/", "").split(
        ":"
    )

    # Override db_host values if specified as env vars.
    project = getattr(settings, "CLOUDSQLSNAPSHOT_PROJECT", db_host_project)
    location = getattr(settings, "CLOUDSQLSNAPSHOT_LOCATION", db_host_location)
    instance = getattr(settings, "CLOUDSQLSNAPSHOT_INSTANCE", db_host_instance)

    # Initiate a CloudSQL snapshot and wait until the operation completes.
    with contextlib.closing(SnapshotClient(project=project)) as client:
        operation = client.start(instance=instance, description=description, location=location)
        logger.info("Snapshot operation submitted.")
        operation_start = time.monotonic()

        while (
            operation["status"] != "DONE"
            and time.monotonic() < operation_start + timeout_duration_secs
        ):
            logger.info(f"Snapshot operation status: {operation['status']}")
            time.sleep(sleep_duration_secs)
            operation = client.get_operation(operation["name"])

    if "error" in operation:
        for error in operation["error"]["errors"]:
            logger.error(error["message"])
        raise SnapshotError("Snapshot operation finished with errors")
    logger.info("Snapshot created successfully.")


# Break getting the database host out into a function for easy mocking in tests.
def _get_db_host(using):
    # "using" refers to the alias of the database specified in the migrate command. We use that to
    # lookup the connection string in the app settings.
    return settings.DATABASES[using]["HOST"]


class SnapshotClient:
    "A simple, mockable, client for creating CloudSQL snapshots."

    def __init__(self, project: str):
        self.service = build("sqladmin", "v1")
        self.project = project

    def start(self, instance: str, description: str, location: str):
        body = {"description": description, "location": location}
        return (
            self.service.backupRuns()
            .insert(project=self.project, instance=instance, body=body)
            .execute()
        )

    def get_operation(self, name: str):
        return self.service.operations().get(project=self.project, operation=name).execute()

    def close(self):
        self.service.close()
